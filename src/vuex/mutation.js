import API from '../api.js';

export default {
  RENDER_MAP(state){
    state.map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: 36.452178,lng: 136.623818},
      streetViewControl: false,
      zoom: 13
    });
  }
}
